package pragma.roosvell.cleanarchitecture.dominio.image.usecases;

import org.junit.jupiter.api.Test;
import org.springframework.web.multipart.MultipartFile;
import pragma.roosvell.cleanarchitecture.application.image.ImageRepository;
import pragma.roosvell.cleanarchitecture.application.person.PersonRepository;
import pragma.roosvell.cleanarchitecture.data.DataImage;
import pragma.roosvell.cleanarchitecture.data.DataPerson;
import pragma.roosvell.cleanarchitecture.dominio.image.model.Image;
import pragma.roosvell.cleanarchitecture.infrastructure.error.NotFoundExc;
import pragma.roosvell.cleanarchitecture.infrastructure.image.entities.ImageEntity;

import java.io.IOException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class ServiceUpdateImageTest {
    ImageRepository imageRepository= mock(ImageRepository.class);
    PersonRepository personRepository = mock(PersonRepository.class);
    ServiceUpdateImage service = new ServiceUpdateImage(imageRepository,personRepository);

    @Test
    void updateImage() throws IOException {
        when(imageRepository.getImageById("id1")).thenReturn(DataImage.createImage1().get());
        when(imageRepository.updateImage(DataImage.createMultipartfile(),"id1",1L))
                .thenReturn(DataImage.createImage1().get());
        when(personRepository.getPersonById(1L)).thenReturn(DataPerson.createPerson1().get());
        Image imagen1 = service.updateImage(DataImage.createMultipartfile(),"id1", 1L );
        assertThrows(NotFoundExc.class, ()->{
            service.updateImage(DataImage.createMultipartfile(),"id3", 1L);
        });
        assertThrows(NotFoundExc.class, ()->{
            service.updateImage(DataImage.createMultipartfile(),"id1", 3L);
        });
    }
}