package pragma.roosvell.cleanarchitecture.infrastructure.error;

public class Error{
    String exceptionName;
    String message;

    public Error(String exceptionName, String message) {
        this.exceptionName = exceptionName;
        this.message = message;
    }

    public String getExceptionName() {
        return exceptionName;
    }

    public String getMessage() {
        return message;
    }


}
