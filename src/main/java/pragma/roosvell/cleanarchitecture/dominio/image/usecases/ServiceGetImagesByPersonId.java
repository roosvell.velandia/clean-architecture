package pragma.roosvell.cleanarchitecture.dominio.image.usecases;

import pragma.roosvell.cleanarchitecture.application.image.ImageRepository;
import pragma.roosvell.cleanarchitecture.application.person.PersonRepository;
import pragma.roosvell.cleanarchitecture.dominio.image.model.Image;
import pragma.roosvell.cleanarchitecture.infrastructure.error.NotFoundExc;

import java.util.List;

public class ServiceGetImagesByPersonId {
    private final ImageRepository imageRepository;
    private final PersonRepository personRepository;

    public ServiceGetImagesByPersonId(ImageRepository imageRepository, PersonRepository personRepository) {
        this.imageRepository = imageRepository;
        this.personRepository = personRepository;
    }
    public List<Image> getImagesByPersonId (Long id) throws NotFoundExc {
     if(personRepository.getPersonById(id)==null){
       throw new NotFoundExc("Person " + id + " Not Found");
     } else {
         return this.imageRepository.getImagesByPersonId(id);
     }

    }
}
