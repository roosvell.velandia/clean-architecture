package pragma.roosvell.cleanarchitecture.infrastructure.person.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import pragma.roosvell.cleanarchitecture.application.person.PersonRepository;
import pragma.roosvell.cleanarchitecture.data.DataPerson;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ControllerDeletePersonById.class)
class ControllerDeletePersonByIdTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    PersonRepository service;

    @Test
    void deletePersonById() throws Exception {
        when(service.deletePerson(1L)).thenReturn("person deleted");
        mvc.perform(delete("/api/person/1").contentType(MediaType.APPLICATION_JSON)
                        .queryParam("id", String.valueOf(1L)))
                .andExpect(status().isOk())
                .andDo(print());
    }
}