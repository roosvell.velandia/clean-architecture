package pragma.roosvell.cleanarchitecture.infrastructure.person.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import pragma.roosvell.cleanarchitecture.application.mapper.PersonMapper;
import pragma.roosvell.cleanarchitecture.application.person.PersonRepository;
import pragma.roosvell.cleanarchitecture.data.DataPerson;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ControllerUpdatePerson.class)
class ControllerUpdatePersonTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    PersonRepository service;

    @MockBean
    PersonMapper personMapper;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void updatePerson() throws Exception {
        Long id = 1L;
        when(service.updatePerson(any(),anyLong()))
                .thenReturn(DataPerson.createPerson1().get());
        mvc.perform(put("/api/person").contentType(MediaType.APPLICATION_JSON)
                .queryParam("id", objectMapper.writeValueAsString(id))
                        .content(objectMapper.writeValueAsString(DataPerson.createPerson1().get())))
                        .andDo(print());
    }
}