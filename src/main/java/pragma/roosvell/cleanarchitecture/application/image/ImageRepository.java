package pragma.roosvell.cleanarchitecture.application.image;

import org.springframework.web.multipart.MultipartFile;
import pragma.roosvell.cleanarchitecture.dominio.image.model.Image;
import pragma.roosvell.cleanarchitecture.infrastructure.error.NotFoundExc;

import java.io.IOException;
import java.util.List;

public interface ImageRepository {
    Image createImage (MultipartFile imageFile, Long idPerson) throws IOException, NotFoundExc;
    Image updateImage (MultipartFile imageFile, String id, Long idPerson) throws IOException, NotFoundExc;
    List<Image> getImages ();
    Image getImageById (String id) throws NotFoundExc;
    String deleteImage (String id) throws NotFoundExc;
    List<Image> getImagesByPersonId (Long id) throws NotFoundExc;
    void updatePerson(String _id, Long idPerson) throws NotFoundExc;
}
