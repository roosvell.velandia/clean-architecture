package pragma.roosvell.cleanarchitecture.dominio.person.usecases;

import org.junit.jupiter.api.Test;
import pragma.roosvell.cleanarchitecture.application.person.PersonRepository;
import pragma.roosvell.cleanarchitecture.data.DataPerson;
import pragma.roosvell.cleanarchitecture.dominio.person.model.Person;
import pragma.roosvell.cleanarchitecture.infrastructure.error.NotFoundExc;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ServiceUpdatePersonTest {

    PersonRepository repository = mock(PersonRepository.class);
    ServiceUpdatePerson service = new ServiceUpdatePerson(repository);

    @Test
    void updatePerson() throws Exception {
        when(repository.getPersonById(1L)).thenReturn(DataPerson.createPerson1().get());
        when(repository.updatePerson(DataPerson.createPerson1().get(),1L))
                .thenReturn(DataPerson.createPerson1().get());
        assertDoesNotThrow( ()->{
            service.updatePerson(DataPerson.createPerson1().get(), 1L);
        });
        assertThrows(NotFoundExc.class, ()->{
            service.updatePerson(DataPerson.createPerson1().get(),3L);
        });
    }
}