package pragma.roosvell.cleanarchitecture.infrastructure.image.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import pragma.roosvell.cleanarchitecture.application.image.ImageRepository;
import pragma.roosvell.cleanarchitecture.application.mapper.ImageMapper;
import pragma.roosvell.cleanarchitecture.data.DataImage;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ControllerUpdateImage.class)
class ControllerUpdateImageTest {
    @MockBean
    ImageRepository service;

    @MockBean
    ImageMapper imageMapper;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void updateImage() throws Exception {
        when(service.updateImage(any(),anyString(), anyLong()))
                .thenReturn(DataImage.createImage1().get());
        mvc.perform(multipart("/api/image").file((MockMultipartFile) DataImage.createMultipartfile())
                        .contentType(MediaType.APPLICATION_JSON)
                        .queryParam("_id", objectMapper.writeValueAsString("id1"))
                        .queryParam("idPerson", objectMapper.writeValueAsString(1L)))
                .andDo(print());
    }
}