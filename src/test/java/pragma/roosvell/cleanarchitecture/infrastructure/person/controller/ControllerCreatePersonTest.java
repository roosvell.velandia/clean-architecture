package pragma.roosvell.cleanarchitecture.infrastructure.person.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import pragma.roosvell.cleanarchitecture.application.mapper.PersonMapper;
import pragma.roosvell.cleanarchitecture.application.person.PersonRepository;
import pragma.roosvell.cleanarchitecture.data.DataPerson;
import pragma.roosvell.cleanarchitecture.dominio.person.model.Person;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ControllerCreatePerson.class)
class ControllerCreatePersonTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    PersonRepository service;

    @MockBean
    PersonMapper personMapper;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void createPerson() throws Exception {
        when(service.createPerson(any()))
                .thenReturn(DataPerson.createPerson1().get());
        mvc.perform(post("/api/person").contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(DataPerson.createPerson1().get())))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.dni").value("12345"))
                        .andDo(print());
    }
}