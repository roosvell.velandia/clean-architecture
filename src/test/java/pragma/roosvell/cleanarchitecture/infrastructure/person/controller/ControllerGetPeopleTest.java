package pragma.roosvell.cleanarchitecture.infrastructure.person.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.web.JsonPath;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import pragma.roosvell.cleanarchitecture.application.person.PersonRepository;
import pragma.roosvell.cleanarchitecture.data.DataPerson;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ControllerGetPeople.class)
class ControllerGetPeopleTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    PersonRepository service;

    @Test
    void getPeople() throws Exception {
        when(service.getPeople()).thenReturn(DataPerson.createPeople());
        mvc.perform(get("/api/person").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("[0].dni").value("12345"));
    }
}