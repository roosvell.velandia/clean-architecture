package pragma.roosvell.cleanarchitecture.dominio.image.usecases;

import pragma.roosvell.cleanarchitecture.application.image.ImageRepository;
import pragma.roosvell.cleanarchitecture.dominio.image.model.Image;
import pragma.roosvell.cleanarchitecture.infrastructure.error.NotFoundExc;

public class ServiceGetImageById {
    private final ImageRepository imageRepository;

    public ServiceGetImageById(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    public Image getImageById (String id) throws NotFoundExc {
        if (this.imageRepository.getImageById(id) == null){
            throw new NotFoundExc("Image " + id + " Not found.");
        }else {
            return this.imageRepository.getImageById(id);
        }
    }
}
