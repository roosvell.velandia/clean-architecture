package pragma.roosvell.cleanarchitecture.infrastructure.person.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pragma.roosvell.cleanarchitecture.application.dto.PersonDTO;
import pragma.roosvell.cleanarchitecture.application.mapper.PersonMapper;
import pragma.roosvell.cleanarchitecture.application.person.PersonRepository;
import pragma.roosvell.cleanarchitecture.dominio.person.model.Person;

@RestController
@RequestMapping("/api/person")
@RequiredArgsConstructor
public class ControllerCreatePerson {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    PersonMapper personMapper;

    @ApiOperation(value="API to create people in MySQL database",response=Person.class)
    @ApiResponses(value={
            @ApiResponse(code=200,message="Person Details Retrieved",response=Person.class),
            @ApiResponse(code=201,message="Person created"),
            @ApiResponse(code=401,message="User unauthorized"),
            @ApiResponse(code=403,message="Operation forbidden"),
            @ApiResponse(code=404,message="Resource not found")
    })
    @PostMapping
    public ResponseEntity<Person> createPerson(@RequestBody PersonDTO personDTO){
        return ResponseEntity.ok().body(personRepository.createPerson(personMapper.personDtoToPerson(personDTO)));
    }

}
