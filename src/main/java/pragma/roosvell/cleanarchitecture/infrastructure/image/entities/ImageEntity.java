package pragma.roosvell.cleanarchitecture.infrastructure.image.entities;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;
import pragma.roosvell.cleanarchitecture.infrastructure.person.entities.PersonEntity;

import javax.persistence.*;

@Setter
@Getter
@Data
@RequiredArgsConstructor
@Document(collection = "ImagesMongodb")
public class ImageEntity {

    @Id
    private String _id;
    private String imageBase64;
    private PersonEntity person;
}
