package pragma.roosvell.cleanarchitecture.dominio.image.usecases;

import pragma.roosvell.cleanarchitecture.application.image.ImageRepository;
import pragma.roosvell.cleanarchitecture.infrastructure.error.NotFoundExc;

public class ServiceDeleteImage {
    private final ImageRepository imageRepository;

    public ServiceDeleteImage(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }
    public String deleteImage (String id) throws NotFoundExc {
        if(imageRepository.getImageById(id) == null){
            throw new NotFoundExc("Image " + id + "Not Found");
        }
        return this.imageRepository.deleteImage(id);
    }
}
