package pragma.roosvell.cleanarchitecture.dominio.person.usecases;

import org.junit.jupiter.api.Test;
import pragma.roosvell.cleanarchitecture.application.person.PersonRepository;
import pragma.roosvell.cleanarchitecture.data.DataPerson;
import pragma.roosvell.cleanarchitecture.dominio.person.model.Person;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


class ServiceGetPeopleTest {

    PersonRepository repository = mock(PersonRepository.class);
    ServiceGetPeople service = new ServiceGetPeople(repository);

    @Test
    void getPeople() throws Exception {
        when(repository.getPeople()).thenReturn(DataPerson.createPeople());
        when(repository.getPersonById(1L)).thenReturn(DataPerson.createPerson1().get());
        when(repository.getPersonById(2L)).thenReturn(DataPerson.createPerson2().get());
        List<Person> people = service.getPeople();
        for (Person person:people){
            repository.getPersonById(person.getId());
            assertNotNull(person);
        }
    }

}