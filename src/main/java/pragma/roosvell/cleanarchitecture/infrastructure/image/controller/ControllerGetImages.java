package pragma.roosvell.cleanarchitecture.infrastructure.image.controller;


import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pragma.roosvell.cleanarchitecture.application.image.ImageRepository;
import pragma.roosvell.cleanarchitecture.dominio.image.model.Image;
import java.util.List;

@RestController
@RequestMapping("/api/image")
@RequiredArgsConstructor
public class ControllerGetImages {

    @Autowired
    ImageRepository imageRepository;

    @ApiOperation(value="API to obtain images from Mongodb database",response= Image.class)
    @ApiResponses(value={
            @ApiResponse(code=200,message="Images details retrieved",response=Image.class),
            @ApiResponse(code=201,message="Query executed successfully"),
            @ApiResponse(code=401,message="User unauthorized"),
            @ApiResponse(code=403,message="Operation forbidden"),
            @ApiResponse(code=404,message="Resource not found")
    })
    @GetMapping
    public ResponseEntity<List<Image>> getImages(){
        return ResponseEntity.ok().body(imageRepository.getImages());
    }
}
