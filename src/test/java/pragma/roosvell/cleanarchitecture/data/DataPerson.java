package pragma.roosvell.cleanarchitecture.data;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import pragma.roosvell.cleanarchitecture.dominio.person.model.Person;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
@Data
@Getter
@Setter
public class DataPerson {
        public static List<Person> createPeople() {
            return Arrays.asList(new Person(1L, 12345L, "roosvell", "Velandia"),
                    new Person(2L, 54321L, "Andrea", "Jimenez"));
        }

        public static final Optional<Person> createPerson1() {
            return Optional.of(new Person(1L, 12345L, "roosvell", "Velandia"));
        }

            public static final Optional<Person> createPerson2() {
                return Optional.of(new Person(2L, 54321L, "Andrea", "Jimenez"));
            }

            public static final Optional<Person> createPerson3() {
                return Optional.of(new Person(3L, 11111L, "Rafael", "suarez"));
            }
}
