package pragma.roosvell.cleanarchitecture.infrastructure.person.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pragma.roosvell.cleanarchitecture.application.person.PersonRepository;
import pragma.roosvell.cleanarchitecture.dominio.person.model.Person;

import java.util.List;

@RestController
@RequestMapping("/api/person")
@RequiredArgsConstructor
public class ControllerGetPeople {

    @Autowired
    PersonRepository personRepository;

    @ApiOperation(value="API to Get people in MySQL database",response=Person.class)
    @ApiResponses(value={
            @ApiResponse(code=200,message="People Details Retrieved",response=Person.class),
            @ApiResponse(code=201,message="Query well executed"),
            @ApiResponse(code=401,message="User unauthorized"),
            @ApiResponse(code=403,message="Operation forbidden"),
            @ApiResponse(code=404,message="Resource not found")
    })
    @GetMapping
    public ResponseEntity<List<Person>> getPeople(){
        return ResponseEntity.ok().body(personRepository.getPeople());
    }
}
