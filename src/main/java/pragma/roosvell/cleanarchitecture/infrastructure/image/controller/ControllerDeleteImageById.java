package pragma.roosvell.cleanarchitecture.infrastructure.image.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pragma.roosvell.cleanarchitecture.application.image.ImageRepository;
import pragma.roosvell.cleanarchitecture.application.mapper.ImageMapper;
import pragma.roosvell.cleanarchitecture.dominio.person.model.Person;


@RestController
@RequestMapping("/api/image")
@RequiredArgsConstructor
public class ControllerDeleteImageById {

    @Autowired
    ImageRepository imageRepository;

    @Autowired
    ImageMapper imageMapper;

    @ApiOperation(value="API to delete images from Mongodb database",response= String.class)
    @ApiResponses(value={
            @ApiResponse(code=200,message="Image deleted correctly",response=String.class),
            @ApiResponse(code=204,message="Image deleted"),
            @ApiResponse(code=400,message="Image not found"),
            @ApiResponse(code=401,message="User unauthorized"),
            @ApiResponse(code=403,message="Operation forbidden"),
            @ApiResponse(code=404,message="Resource not found")
    })
    @DeleteMapping("{id}")
    public void deleteImageById(String id){
        imageRepository.deleteImage(id);
    }
}
