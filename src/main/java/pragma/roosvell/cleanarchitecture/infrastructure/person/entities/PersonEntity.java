package pragma.roosvell.cleanarchitecture.infrastructure.person.entities;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import javax.persistence.*;

@Data
@Getter
@Setter
@Entity
@Table(name = PersonEntity.TABLE)
public class PersonEntity {
    public static final String TABLE = "person";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The database generated person ID")
    private Long id;

    //@Column nombre de la columna en base de datos

    @Column(name = "dni")
    @ApiModelProperty(notes = "fill with person's DNI ")
    private Long dni;

    @Column(name = "name")
    @ApiModelProperty(notes = "Save name ")
    private String name;

    @Column(name = "lastname")
    @ApiModelProperty(notes = "Save Lastname ")
    private String lastname;

}
