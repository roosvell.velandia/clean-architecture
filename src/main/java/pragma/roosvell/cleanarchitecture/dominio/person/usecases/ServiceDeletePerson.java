package pragma.roosvell.cleanarchitecture.dominio.person.usecases;

import pragma.roosvell.cleanarchitecture.application.person.PersonRepository;
import pragma.roosvell.cleanarchitecture.infrastructure.error.NotFoundExc;

public class ServiceDeletePerson {
    private final PersonRepository personRepository;
    public ServiceDeletePerson(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public String deletePerson (Long id) throws NotFoundExc {
        if (personRepository.getPersonById(id) == null){
            throw new NotFoundExc("Person " + id + "Not Found");
        }
        return this.personRepository.deletePerson(id);
    }
}
