package pragma.roosvell.cleanarchitecture.dominio.person.usecases;

import pragma.roosvell.cleanarchitecture.dominio.person.model.Person;
import pragma.roosvell.cleanarchitecture.application.person.PersonRepository;

public class ServiceCreatePerson {
    private final PersonRepository personRepository;

    public ServiceCreatePerson(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public Person createPerson (Person person){
        return this.personRepository.createPerson(person);
    }
}
