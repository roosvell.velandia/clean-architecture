package pragma.roosvell.cleanarchitecture.dominio.person.usecases;

import pragma.roosvell.cleanarchitecture.application.person.PersonRepository;
import pragma.roosvell.cleanarchitecture.dominio.person.model.Person;
import pragma.roosvell.cleanarchitecture.infrastructure.error.NotFoundExc;

public class ServiceGetPersonById {
    private final PersonRepository personRepository;

    public ServiceGetPersonById(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public Person getPersonById (Long id) throws NotFoundExc{
        if (this.personRepository.getPersonById(id) == null){
            throw new NotFoundExc("Person " + id + " Not found.");
        }else{
            return this.personRepository.getPersonById(id);
        }

    }
}
