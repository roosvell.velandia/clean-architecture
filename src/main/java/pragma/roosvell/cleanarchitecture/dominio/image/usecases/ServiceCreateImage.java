package pragma.roosvell.cleanarchitecture.dominio.image.usecases;

import org.springframework.web.multipart.MultipartFile;
import pragma.roosvell.cleanarchitecture.application.image.ImageRepository;
import pragma.roosvell.cleanarchitecture.application.person.PersonRepository;
import pragma.roosvell.cleanarchitecture.dominio.image.model.Image;
import pragma.roosvell.cleanarchitecture.infrastructure.error.NotFoundExc;

import java.io.IOException;

public class ServiceCreateImage {
    private final ImageRepository imageRepository;
    private final PersonRepository personRepository;

    public ServiceCreateImage(ImageRepository imageRepository, PersonRepository personRepository) {
        this.imageRepository = imageRepository;
        this.personRepository = personRepository;
    }

    public Image createImage (MultipartFile file, Long idPerson) throws IOException, NotFoundExc {
        if(personRepository.getPersonById(idPerson) == null){
            throw new NotFoundExc("person " + idPerson + " Not Found");
        }
        return this.imageRepository.createImage(file, idPerson);
    }
}
