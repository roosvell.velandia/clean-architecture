package pragma.roosvell.cleanarchitecture.infrastructure.error;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.concurrent.ConcurrentHashMap;

@RestControllerAdvice
public class ErrorHandler extends ResponseEntityExceptionHandler {

    private static final String ERROR_UNHANDLABLE = "Please contact Administrator.";
    private static final ConcurrentHashMap<String, Integer> STATUS_CODE = new ConcurrentHashMap<>();

    public ErrorHandler() {
        STATUS_CODE.put(
                NotFoundExc.class.getSimpleName(), HttpStatus.BAD_REQUEST.value()
        );
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Error> handleAllExceptions(Exception exception) {
        ResponseEntity<Error> result;

        String exceptionName = exception.getClass().getSimpleName();
        String message = exception.getMessage();
        Integer code = STATUS_CODE.get(exceptionName);

        if (code != null) {
            Error error = new Error(exceptionName, message);
            result = new ResponseEntity<>(error, HttpStatus.valueOf(code));
        }
        else {
            Error error = new Error(exceptionName, ERROR_UNHANDLABLE);
            result = new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
        }
        return result;
    }

}
