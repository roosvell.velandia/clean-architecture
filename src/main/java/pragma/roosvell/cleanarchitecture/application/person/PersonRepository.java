package pragma.roosvell.cleanarchitecture.application.person;

import pragma.roosvell.cleanarchitecture.dominio.person.model.Person;
import pragma.roosvell.cleanarchitecture.infrastructure.error.NotFoundExc;

import java.util.List;

public interface PersonRepository {
    Person createPerson (Person person);
    Person updatePerson (Person person, Long id) throws NotFoundExc;
    List<Person> getPeople ();
    Person getPersonById (Long id) throws NotFoundExc;
    String deletePerson (Long id) throws NotFoundExc;
}
