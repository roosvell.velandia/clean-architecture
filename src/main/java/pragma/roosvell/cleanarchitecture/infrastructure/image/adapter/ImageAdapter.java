package pragma.roosvell.cleanarchitecture.infrastructure.image.adapter;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pragma.roosvell.cleanarchitecture.application.image.ImageRepository;
import pragma.roosvell.cleanarchitecture.application.mapper.ImageMapper;
import pragma.roosvell.cleanarchitecture.application.mapper.PersonMapper;
import pragma.roosvell.cleanarchitecture.dominio.image.model.Image;
import pragma.roosvell.cleanarchitecture.dominio.person.model.Person;
import pragma.roosvell.cleanarchitecture.infrastructure.error.NotFoundExc;
import pragma.roosvell.cleanarchitecture.infrastructure.image.entities.ImageEntity;
import pragma.roosvell.cleanarchitecture.infrastructure.image.imagedao.ImageDao;
import pragma.roosvell.cleanarchitecture.infrastructure.person.entities.PersonEntity;
import pragma.roosvell.cleanarchitecture.infrastructure.person.persondao.PersonDao;
import java.io.IOException;
import java.util.Base64;
import java.util.List;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public class ImageAdapter implements ImageRepository {
    private static String personNf = "Person Not Found ";
    private static String registerNf = "Register Not Found ";
    private final ImageDao imageDao;
    private final ImageMapper imageMapper;
    private final PersonDao personDao;
    private final PersonMapper personMapper;

    @Override
    public Image createImage(MultipartFile imageFile, Long idPerson) throws IOException, NotFoundExc {
        ImageEntity image = new ImageEntity();
        //convierto la imagen en un mapa de bits y lo transformo en base 64
        byte[] fileContent = imageFile.getBytes();
        String encodedString = Base64
                .getEncoder()
                .encodeToString(fileContent);
        image.setImageBase64(encodedString);
        Optional<PersonEntity> persondb = this.personDao.findById(idPerson);
        if (persondb.isPresent()) {
            PersonEntity personupdate = persondb.get();
            // actualizo los datos en la imagen
            image.setPerson((personupdate));

        } else {
            throw new NotFoundExc(personNf + idPerson);
        }
        imageDao.save(image);
        return imageMapper.imageEntityToImage(image);
    }

    @Override
    public Image updateImage(MultipartFile imageFile, String id, Long idPerson) throws IOException, NotFoundExc {
        Optional <ImageEntity> imagedb= this.imageDao.findById(id);
        Optional<PersonEntity> persondb = this.personDao.findById(idPerson);
        // busco si existe la persona
        if (persondb.isPresent()) {
            Person personupdate = personMapper.entityToPerson(persondb.get());
            // actualizo los datos en la imagen
            if (imagedb.isPresent()) {
                ImageEntity imageUpdate = imagedb.get();
                imageUpdate.setPerson(personMapper.personToEntity(personupdate));
                byte[] fileContent = imageFile.getBytes();
                String encodedString = Base64
                        .getEncoder()
                        .encodeToString(fileContent);
                imageUpdate.setImageBase64(encodedString);
                // devuelvo la imagen actualizada.
                imageDao.save(imageUpdate);
                return imageMapper.imageEntityToImage(imageUpdate);
            } else {
                throw new NotFoundExc(registerNf + id);
            }
        } else {
            throw new NotFoundExc(personNf + idPerson);
        }
        //convierto la imagen en un mapa de bits y lo transformo en base 64

    }

    @Override
    public List<Image> getImages() {
        return imageMapper.listImageEntityToListImage(imageDao.findAll());
    }

    @Override
    public Image getImageById(String id) throws NotFoundExc {
        Optional<ImageEntity> imagedb = this.imageDao.findById(id);
        if(((Optional<?>) imagedb).isPresent()){
            return imageMapper.imageEntityToImage(imagedb.get());
        } else {
            throw new NotFoundExc(registerNf + id);
        }
    }

    @Override
    public String deleteImage(String id) throws NotFoundExc {
        Optional<ImageEntity> imagedb = this.imageDao.findById(id);
        if(((Optional<?>) imagedb).isPresent()){
            imageDao.delete(imagedb.get());
            return "image deleted";
        } else {
            throw new NotFoundExc(registerNf + id);
        }
    }

    @Override
    public List<Image> getImagesByPersonId(Long id) {
        Optional<PersonEntity> persondb = this.personDao.findById(id);
        if (persondb.isPresent()) {
            return imageMapper.listImageEntityToListImage(imageDao.findByPersonId(id));
        } else {
            throw new NotFoundExc(personNf + id);
        }
    }

    @Override
    public void updatePerson(String _id, Long idPerson) throws NotFoundExc{

        Optional<PersonEntity> persondb = this.personDao.findById(idPerson);
        // busco si existe la persona
        if (persondb.isPresent()) {
            Person personupdate = personMapper.entityToPerson(persondb.get());
            Optional <ImageEntity> imagedb= this.imageDao.findById(_id);
            if (imagedb.isPresent()) {
                ImageEntity imageUpdate = imagedb.get();
                // actualizo los datos en la imagen
                imageUpdate.setPerson(personMapper.personToEntity(personupdate));
                // devuelvo la imagen actualizada.
                imageMapper.imageEntityToImage(imageDao.save(imageUpdate));
            } else {
                throw new NotFoundExc(registerNf + _id);
            }

        } else {
            throw new NotFoundExc(personNf + idPerson);
        }

    }
}
