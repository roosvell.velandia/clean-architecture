package pragma.roosvell.cleanarchitecture.infrastructure.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pragma.roosvell.cleanarchitecture.application.person.PersonRepository;
import pragma.roosvell.cleanarchitecture.dominio.person.usecases.*;

@Configuration
public class ServicesConfiguration {

    @Bean
    public ServiceCreatePerson serviceCreatePerson (PersonRepository personRepository){
        return new ServiceCreatePerson(personRepository);
    }

    @Bean
    public ServiceGetPeople serviceGetPeople(PersonRepository personRepository){
        return new ServiceGetPeople(personRepository);
    }

    @Bean
    public ServiceDeletePerson serviceDeletePerson (PersonRepository personRepository){
        return new ServiceDeletePerson(personRepository);
    }

    @Bean
    public ServiceGetPersonById serviceGetPersonById (PersonRepository personRepository){
        return new ServiceGetPersonById(personRepository);
    }

    @Bean
    public ServiceUpdatePerson serviceUpdatePerson (PersonRepository personRepository){
        return new ServiceUpdatePerson(personRepository);
    }


}
