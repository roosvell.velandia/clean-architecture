package pragma.roosvell.cleanarchitecture.infrastructure.image.imagedao;

import org.springframework.data.mongodb.repository.MongoRepository;
import pragma.roosvell.cleanarchitecture.infrastructure.image.entities.ImageEntity;

import java.util.List;

public interface ImageDao extends MongoRepository<ImageEntity,String> {
    List<ImageEntity> findByPersonId(Long id);
}
