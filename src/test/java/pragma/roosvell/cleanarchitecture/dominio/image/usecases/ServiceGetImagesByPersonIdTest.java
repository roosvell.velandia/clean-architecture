package pragma.roosvell.cleanarchitecture.dominio.image.usecases;

import javassist.NotFoundException;
import org.junit.jupiter.api.Test;
import pragma.roosvell.cleanarchitecture.application.image.ImageRepository;
import pragma.roosvell.cleanarchitecture.application.person.PersonRepository;
import pragma.roosvell.cleanarchitecture.data.DataImage;
import pragma.roosvell.cleanarchitecture.data.DataPerson;
import pragma.roosvell.cleanarchitecture.dominio.image.model.Image;
import pragma.roosvell.cleanarchitecture.dominio.person.usecases.ServiceGetPersonById;
import pragma.roosvell.cleanarchitecture.infrastructure.error.NotFoundExc;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class ServiceGetImagesByPersonIdTest {
    PersonRepository personRepository = mock(PersonRepository.class);
    ImageRepository imageRepository = mock(ImageRepository.class);
    ServiceGetImages serviceGetImages = mock(ServiceGetImages.class);
    ServiceGetImageById serviceGetImageById = mock(ServiceGetImageById.class);
    ServiceGetImagesByPersonId service = new ServiceGetImagesByPersonId(imageRepository,personRepository);


    @Test
    void getImagesByPersonId() throws NotFoundExc {
        when(imageRepository.getImagesByPersonId(DataPerson.createPerson1().get().getId()))
                .thenReturn(DataImage.createImages());
        when(serviceGetImages.getImages()).thenReturn(DataImage.createImages());
        when(serviceGetImageById.getImageById("id1")).thenReturn(DataImage.createImage1().get());
        when(serviceGetImageById.getImageById("id2")).thenReturn(DataImage.createImage2().get());
        when(personRepository.getPersonById(1L)).thenReturn(DataPerson.createPerson1().get());

        List<Image> images = service.getImagesByPersonId(DataPerson.createPerson1().get().getId());
        for (Image image:images){
            serviceGetImageById.getImageById(image.get_id());
            assertNotNull(image);
        }
        assertThrows(NotFoundExc.class, ()->{
            service.getImagesByPersonId(3L);
        });

        verify(imageRepository, times(1)).getImagesByPersonId(1L);
        verify(imageRepository, times(1)).getImagesByPersonId(any(Long.class));
    }
}