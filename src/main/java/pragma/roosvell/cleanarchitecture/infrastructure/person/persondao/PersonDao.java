package pragma.roosvell.cleanarchitecture.infrastructure.person.persondao;

import org.springframework.data.jpa.repository.JpaRepository;
import pragma.roosvell.cleanarchitecture.infrastructure.person.entities.PersonEntity;

public interface PersonDao extends JpaRepository<PersonEntity,Long> {
}
