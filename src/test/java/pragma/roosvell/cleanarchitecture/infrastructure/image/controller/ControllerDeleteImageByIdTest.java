package pragma.roosvell.cleanarchitecture.infrastructure.image.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import pragma.roosvell.cleanarchitecture.application.image.ImageRepository;
import pragma.roosvell.cleanarchitecture.application.mapper.ImageMapper;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ControllerDeleteImageById.class)
class ControllerDeleteImageByIdTest {

    @MockBean
    ImageRepository service;

    @MockBean
    ImageMapper imageMapper;

    @Autowired
    private MockMvc mvc;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    void deleteImageById() throws Exception {
        when(service.deleteImage("id1")).thenReturn("image deleted");
        mvc.perform(delete("/api/image/id1").contentType(MediaType.APPLICATION_JSON)
                        .queryParam("id", "id1"))
                .andExpect(status().isOk())
                .andDo(print());
    }
}