package pragma.roosvell.cleanarchitecture.infrastructure.person.adapter;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import pragma.roosvell.cleanarchitecture.application.image.ImageRepository;
import pragma.roosvell.cleanarchitecture.application.mapper.PersonMapper;
import pragma.roosvell.cleanarchitecture.application.person.PersonRepository;
import pragma.roosvell.cleanarchitecture.dominio.image.model.Image;
import pragma.roosvell.cleanarchitecture.dominio.person.model.Person;
import pragma.roosvell.cleanarchitecture.infrastructure.error.NotFoundExc;
import pragma.roosvell.cleanarchitecture.infrastructure.person.persondao.PersonDao;
import pragma.roosvell.cleanarchitecture.infrastructure.person.entities.PersonEntity;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Repository
public class PersonAdapter implements PersonRepository {
    private static String personNf = "Person Not Found ";
    private final PersonDao persondao;
    private final PersonMapper personMapper;
    private final ImageRepository imagedao;

    @Override
    public Person createPerson(Person person) {
        return personMapper.entityToPerson(persondao.save(personMapper.personToEntity(person)));
    }

    @Override
    public Person updatePerson(Person person, Long id) throws NotFoundExc {
        Optional<PersonEntity> personDB= persondao.findById(id);
        if (personDB.isPresent()) {
            PersonEntity personaUpdate = personDB.get();
            personaUpdate.setId(id);
            personaUpdate.setLastname(person.getLastname());
            personaUpdate.setName(person.getName());
            personaUpdate.setDni(person.getDni());
            persondao.save(personaUpdate);
            List<Image> personImages = imagedao.getImagesByPersonId(id);
            for(Image personImage : personImages){
                imagedao.updatePerson(personImage.get_id(),id);
            }
            return personMapper.entityToPerson(personaUpdate);
        } else {
            throw new NotFoundExc(personNf + id);
        }
    }

    @Override
    public List<Person> getPeople() {
        return personMapper.listEntityToListPerson(persondao.findAll());
    }

    @Override
    public Person getPersonById(Long id) throws NotFoundExc {
        Optional<PersonEntity> personDB= persondao.findById(id);
        if (personDB.isPresent()) {
            return personMapper.entityToPerson(personDB.get());
        } else
        {
            throw new NotFoundExc(personNf + id );
        }

    }

    @Override
    public String deletePerson(Long id) throws NotFoundExc {
        Optional<PersonEntity> personDB= persondao.findById(id);
        if (personDB.isPresent()) {
            List<Image> personImages = imagedao.getImagesByPersonId(id);
            for(Image personImage : personImages){
                imagedao.deleteImage(personImage.get_id());
            }
            persondao.deleteById(id);
            return "Person deleted";
        } else
        {
            throw new NotFoundExc(personNf + id);
        }

    }
}
