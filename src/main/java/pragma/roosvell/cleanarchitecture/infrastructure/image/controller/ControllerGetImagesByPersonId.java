package pragma.roosvell.cleanarchitecture.infrastructure.image.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pragma.roosvell.cleanarchitecture.application.image.ImageRepository;
import pragma.roosvell.cleanarchitecture.dominio.image.model.Image;

import java.util.List;

@RestController
@RequestMapping("/api/images")
@RequiredArgsConstructor
public class ControllerGetImagesByPersonId {
    @Autowired
    ImageRepository imageRepository;

    @ApiOperation(value="API to obtain image by person id from Mongodb database",response= Image.class)
    @ApiResponses(value={
            @ApiResponse(code=200,message="images details retrieved",response=Image.class),
            @ApiResponse(code=201,message="Query executed successfully"),
            @ApiResponse(code=400,message="Person not found"),
            @ApiResponse(code=401,message="User unauthorized"),
            @ApiResponse(code=403,message="Operation forbidden"),
            @ApiResponse(code=404,message="Resource not found")
    })
    @GetMapping("{id}")
    public ResponseEntity<List<Image>> getImageByPersonId(Long id)  {
        return ResponseEntity.ok().body(imageRepository.getImagesByPersonId(id));
    }
}
