package pragma.roosvell.cleanarchitecture.dominio.image.usecases;

import javassist.NotFoundException;
import org.junit.jupiter.api.Test;
import pragma.roosvell.cleanarchitecture.application.image.ImageRepository;
import pragma.roosvell.cleanarchitecture.data.DataImage;
import pragma.roosvell.cleanarchitecture.infrastructure.error.NotFoundExc;
import pragma.roosvell.cleanarchitecture.infrastructure.image.entities.ImageEntity;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

class ServiceDeleteImageTest {
    ImageRepository repository= mock(ImageRepository.class);
    ServiceDeleteImage service = new ServiceDeleteImage(repository);

    @Test
    void deleteImage() throws NotFoundExc {
        when(repository.getImageById("id1"))
                .thenReturn(DataImage.createImage1().get());
        service.deleteImage("id1");
        assertThrows(NotFoundExc.class, ()->{
            service.deleteImage("id3");
        });
        verify(repository, times(1)).deleteImage("id1");
        verify(repository).deleteImage(any(String.class));
    }
}