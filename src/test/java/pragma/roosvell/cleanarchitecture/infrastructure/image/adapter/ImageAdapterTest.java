package pragma.roosvell.cleanarchitecture.infrastructure.image.adapter;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import pragma.roosvell.cleanarchitecture.application.mapper.ImageMapper;
import pragma.roosvell.cleanarchitecture.application.mapper.PersonMapper;
import pragma.roosvell.cleanarchitecture.data.DataImage;
import pragma.roosvell.cleanarchitecture.data.DataPerson;
import pragma.roosvell.cleanarchitecture.dominio.image.model.Image;
import pragma.roosvell.cleanarchitecture.infrastructure.error.NotFoundExc;
import pragma.roosvell.cleanarchitecture.infrastructure.image.entities.ImageEntity;
import pragma.roosvell.cleanarchitecture.infrastructure.image.imagedao.ImageDao;
import pragma.roosvell.cleanarchitecture.infrastructure.person.persondao.PersonDao;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
class ImageAdapterTest {
    @MockBean
    ImageDao imageDao;

    @MockBean
    PersonDao personDao;

    @Autowired
    ImageMapper imageMapper;

    @Autowired
    PersonMapper personMapper;
    
    @Autowired
    ImageAdapter service; 
    
    @Test
    void createImage() throws IOException {
        when(personDao.findById(1L)).
                thenReturn(Optional.ofNullable(personMapper
                        .personToEntity(DataPerson.createPerson1().get())));
        when(imageDao.save(imageMapper
                .imagetoImageEntity(DataImage.createImage1().get())))
                        .thenReturn(imageMapper.imagetoImageEntity(DataImage.createImage1().get()));
        Image image = service.createImage(DataImage.createMultipartfile(), 1L );
        //assertNotNull(image);
        assertEquals(personDao.findById(1L).get().getId(), image.getPerson().getId());
        assertThrows(NotFoundExc.class, ()->{
            service.createImage(DataImage.createMultipartfile(), 2L );
        });
    }

    @Test
    void updateImage() throws IOException {
        when(imageDao.findById("id1"))
                .thenReturn(Optional.ofNullable(imageMapper
                        .imagetoImageEntity(DataImage.createImage1().get())));
        when(imageDao.save(imageMapper.imagetoImageEntity(DataImage.createImage1().get())))
                .thenReturn(imageMapper.imagetoImageEntity(DataImage.createImage1().get()));
        when(personDao.findById(1L)).thenReturn(Optional.ofNullable(personMapper
                .personToEntity(DataPerson.createPerson1().get())));
        Image image1 = service.updateImage(DataImage.createMultipartfile(),"id1",1L);
        assertEquals("id1",image1.get_id());
        assertEquals(1L,image1.getPerson().getId());
        assertNotEquals(image1.getImageBase64(),DataImage.createImage1().get().getImageBase64());
        assertThrows(NotFoundExc.class, ()->{
            service.updateImage(DataImage.createMultipartfile(),"id1",3L);
        });
        assertThrows(NotFoundExc.class, ()->{
            service.updateImage(DataImage.createMultipartfile(),"id3",1L);
        });
        verify(imageDao).save(any(ImageEntity.class));

    }

    @Test
    void getImages() {
        when(imageDao.findAll())
                .thenReturn(imageMapper
                        .listImageToListImageEntity(DataImage.createImages()));
        when(imageDao.findById("id1"))
                .thenReturn(Optional.ofNullable(imageMapper
                        .imagetoImageEntity(DataImage.createImage1().get())));
        when(imageDao.findById("id2"))
                .thenReturn(Optional.ofNullable(imageMapper
                        .imagetoImageEntity(DataImage.createImage2().get())));
        List<Image> images = service.getImages();
        for (Image image:images){
            service.getImageById(image.get_id());
            assertNotNull(image);
        }
        verify(imageDao).findById("id1");
        verify(imageDao).findById("id2");
        verify(imageDao,times(2)).findById(any(String.class));
        verify(imageDao).findAll();
        

    }

    @Test
    void getImageById() {
        when(imageDao.findAll()).thenReturn(imageMapper
                .listImageToListImageEntity(DataImage.createImages()));
        when(imageDao.findById("id1"))
                .thenReturn(Optional.ofNullable(imageMapper
                        .imagetoImageEntity(DataImage.createImage1().get())));
        when(imageDao.findById("id2"))
                .thenReturn(Optional.ofNullable(imageMapper
                        .imagetoImageEntity(DataImage.createImage2().get())));
        List<Image> images = service.getImages();
        for (Image image:images){
            service.getImageById(image.get_id());
            assertNotNull(image);
        }
        assertThrows(NotFoundExc.class, ()->{
            service.getImageById("id3");
        });
        verify(imageDao, times(1)).findById("id1");
        verify(imageDao, times(1)).findById("id2");
        verify(imageDao,times(3)).findById(any(String.class));
        verify(imageDao,times(1)).findAll();
    }

    @Test
    void deleteImage() {
        when(imageDao.findById("id1"))
                .thenReturn(Optional.ofNullable(imageMapper.imagetoImageEntity(DataImage.createImage1().get())));
        service.deleteImage("id1");
        assertThrows(NotFoundExc.class, ()->{
            service.deleteImage("id3");
        });
        verify(imageDao, times(1)).delete(imageMapper.imagetoImageEntity(DataImage.createImage1().get()));
        verify(imageDao).delete(any(ImageEntity.class));
    }

    @Test
    void getImagesByPersonId() {
        when(imageDao.findByPersonId(DataPerson.createPerson1().get().getId()))
                .thenReturn(imageMapper.listImageToListImageEntity(DataImage.createImages()));
        when(imageDao.findAll()).thenReturn(imageMapper
                .listImageToListImageEntity(DataImage.createImages()));
        when(imageDao.findById("id1"))
                .thenReturn(Optional.ofNullable(imageMapper
                        .imagetoImageEntity(DataImage.createImage1().get())));
        when(imageDao.findById("id2"))
                .thenReturn(Optional.ofNullable(imageMapper
                        .imagetoImageEntity(DataImage.createImage2().get())));
        when(personDao.findById(1L)).
                thenReturn(Optional.ofNullable(personMapper
                        .personToEntity(DataPerson.createPerson1().get())));
        List<Image> images = service.getImagesByPersonId(DataPerson.createPerson1().get().getId());
        for (Image image:images){
            service.getImageById(image.get_id());
            assertNotNull(image);
        }
        assertThrows(NotFoundExc.class, ()->{
            service.getImagesByPersonId(3L);
        });

        verify(imageDao, times(1)).findById("id1");
        verify(imageDao, times(1)).findById("id2");
        verify(imageDao,times(2)).findById(any(String.class));
        verify(imageDao).findByPersonId(any(Long.class));


    }

    @Test
    void updatePerson() {
    }
}