package pragma.roosvell.cleanarchitecture.dominio.image.usecases;

import javassist.NotFoundException;
import org.junit.jupiter.api.Test;
import pragma.roosvell.cleanarchitecture.application.image.ImageRepository;
import pragma.roosvell.cleanarchitecture.application.person.PersonRepository;
import pragma.roosvell.cleanarchitecture.data.DataImage;
import pragma.roosvell.cleanarchitecture.data.DataPerson;
import pragma.roosvell.cleanarchitecture.dominio.image.model.Image;
import pragma.roosvell.cleanarchitecture.infrastructure.error.NotFoundExc;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ServiceCreateImageTest {
    ImageRepository imageRepository= mock(ImageRepository.class);
    PersonRepository personRepository = mock(PersonRepository.class);
    ServiceCreateImage service = new ServiceCreateImage(imageRepository,personRepository);

    @Test
    void createImage() throws IOException, NotFoundExc {
        when(imageRepository.createImage(DataImage.createMultipartfile(),1L)).thenReturn(DataImage.createImage1().get());
        when(personRepository.getPersonById(1L)).thenReturn(DataPerson.createPerson1().get());
        Image imagen1 = service.createImage(DataImage.createMultipartfile(), 1L );
        assertDoesNotThrow( ()->{
            service.createImage(DataImage.createMultipartfile(), 1L);
        });
        assertThrows(NotFoundExc.class, ()->{
            service.createImage(DataImage.createMultipartfile(), 2L);
        });

    }
}