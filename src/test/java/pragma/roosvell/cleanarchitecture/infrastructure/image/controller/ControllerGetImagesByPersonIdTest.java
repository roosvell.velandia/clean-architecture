package pragma.roosvell.cleanarchitecture.infrastructure.image.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import pragma.roosvell.cleanarchitecture.application.image.ImageRepository;
import pragma.roosvell.cleanarchitecture.data.DataImage;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ControllerGetImagesByPersonId.class)
class ControllerGetImagesByPersonIdTest {

    @MockBean
    ImageRepository service;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void getImageByPersonId() throws Exception {
        when(service.getImagesByPersonId(anyLong())).thenReturn(DataImage.createImages());
        mvc.perform(get("/api/images/1").contentType(MediaType.APPLICATION_JSON)
                        .queryParam("id", objectMapper.writeValueAsString(1L)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("[0]._id").value("id1"));


    }
}