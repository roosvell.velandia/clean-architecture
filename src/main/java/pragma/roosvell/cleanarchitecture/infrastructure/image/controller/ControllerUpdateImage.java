package pragma.roosvell.cleanarchitecture.infrastructure.image.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pragma.roosvell.cleanarchitecture.application.image.ImageRepository;
import pragma.roosvell.cleanarchitecture.application.mapper.ImageMapper;
import pragma.roosvell.cleanarchitecture.dominio.image.model.Image;

import java.io.IOException;

@RestController
@RequestMapping("/api/image")
@RequiredArgsConstructor
public class ControllerUpdateImage {

    @Autowired
    ImageRepository imageRepository;

    @Autowired
    ImageMapper imageMapper;

    @ApiOperation(value="API to obtain update images from Mongodb database",response= Image.class)
    @ApiResponses(value={
            @ApiResponse(code=200,message="Image new details retrieved",response=Image.class),
            @ApiResponse(code=201,message="Image updated"),
            @ApiResponse(code=400,message="Data(image or person) not found"),
            @ApiResponse(code=401,message="User unauthorized"),
            @ApiResponse(code=403,message="Operation forbidden"),
            @ApiResponse(code=404,message="Resource not found")
    })
    @PutMapping
    public ResponseEntity<Image> updateImage(@RequestParam("file") MultipartFile imageFile,@RequestParam String idImage, @RequestParam Long idPerson) throws IOException{
        return ResponseEntity.ok().body(imageRepository.updateImage(imageFile,idImage,idPerson));
    }
}
