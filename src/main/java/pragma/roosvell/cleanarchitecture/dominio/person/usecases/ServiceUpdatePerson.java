package pragma.roosvell.cleanarchitecture.dominio.person.usecases;

import pragma.roosvell.cleanarchitecture.application.person.PersonRepository;
import pragma.roosvell.cleanarchitecture.dominio.person.model.Person;
import pragma.roosvell.cleanarchitecture.infrastructure.error.NotFoundExc;

public class ServiceUpdatePerson {
    private final PersonRepository personRepository;

    public ServiceUpdatePerson(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public Person updatePerson (Person person, Long id) throws NotFoundExc {
        if(personRepository.getPersonById(id) == null){
            throw new NotFoundExc("person " + id + "Not Found");
        }
        return this.personRepository.updatePerson(person, id);
    }
}
