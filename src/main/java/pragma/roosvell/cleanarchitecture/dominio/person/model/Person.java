package pragma.roosvell.cleanarchitecture.dominio.person.model;

public class Person {
    private Long id;
    private Long dni;
    private String name;
    private String lastname;

    public Person() {
    }

    public Person(Long id, Long dni, String name, String lastname) {
        this.id = id;
        this.dni = dni;
        this.name = name;
        this.lastname = lastname;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDni() {
        return dni;
    }

    public void setDni(Long dni) {
        this.dni = dni;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

}
