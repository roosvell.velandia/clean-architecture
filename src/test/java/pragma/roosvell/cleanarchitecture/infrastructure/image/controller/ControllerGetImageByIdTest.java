package pragma.roosvell.cleanarchitecture.infrastructure.image.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import pragma.roosvell.cleanarchitecture.application.image.ImageRepository;
import pragma.roosvell.cleanarchitecture.application.mapper.ImageMapper;
import pragma.roosvell.cleanarchitecture.data.DataImage;
import pragma.roosvell.cleanarchitecture.data.DataPerson;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ControllerGetImageById.class)
class ControllerGetImageByIdTest {
    @MockBean
    ImageRepository service;

    @Autowired
    private MockMvc mvc;

    @MockBean
    ImageMapper imageMapper;


    @Test
    void getImageById() throws Exception {
        when(service.getImageById("id1")).thenReturn(DataImage.createImage1().get());
        mvc.perform(get("/api/image/id1").contentType(MediaType.APPLICATION_JSON)
                        .queryParam("id", "id1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._id").value("id1"))
                .andDo(print());

    }
}