package pragma.roosvell.cleanarchitecture.infrastructure.image.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pragma.roosvell.cleanarchitecture.application.image.ImageRepository;
import pragma.roosvell.cleanarchitecture.application.mapper.ImageMapper;
import pragma.roosvell.cleanarchitecture.dominio.image.model.Image;



@RestController
@RequestMapping("/api/image")
@RequiredArgsConstructor
public class ControllerGetImageById {

    @Autowired
    ImageRepository imageRepository;

    @Autowired
    ImageMapper imageMapper;

    @ApiOperation(value="API to obtain image by id from Mongodb database",response= Image.class)
    @ApiResponses(value={
            @ApiResponse(code=200,message="image details retrieved",response=Image.class),
            @ApiResponse(code=201,message="Query executed successfully"),
            @ApiResponse(code=400,message="Image not found"),
            @ApiResponse(code=401,message="User unauthorized"),
            @ApiResponse(code=403,message="Operation forbidden"),
            @ApiResponse(code=404,message="Resource not found")
    })
    @GetMapping("{id}")
    public ResponseEntity<Image> getImageById(String id){
        return ResponseEntity.ok().body(imageRepository.getImageById(id));
    }
}
