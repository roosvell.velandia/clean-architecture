package pragma.roosvell.cleanarchitecture.dominio.image.usecases;

import pragma.roosvell.cleanarchitecture.application.image.ImageRepository;
import pragma.roosvell.cleanarchitecture.dominio.image.model.Image;

import java.util.List;

public class ServiceGetImages {
    private final ImageRepository imageRepository;

    public ServiceGetImages(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    public List<Image> getImages () {
        return this.imageRepository.getImages();
    }
}
