package pragma.roosvell.cleanarchitecture.infrastructure.image.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pragma.roosvell.cleanarchitecture.application.image.ImageRepository;
import pragma.roosvell.cleanarchitecture.dominio.image.model.Image;
import pragma.roosvell.cleanarchitecture.dominio.person.model.Person;

import java.io.IOException;

@RestController
@RequestMapping("/api/image")
@RequiredArgsConstructor
public class ControllerCreateImage {

    @Autowired
    ImageRepository imageRepository;

    @ApiOperation(value="API to Create images in Mongodb database",response= Image.class)
    @ApiResponses(value={
            @ApiResponse(code=200,message="New image details retrieved",response=Image.class),
            @ApiResponse(code=201,message="Image created"),
            @ApiResponse(code=400,message="Person not found"),
            @ApiResponse(code=401,message="User unauthorized"),
            @ApiResponse(code=403,message="Operation forbidden"),
            @ApiResponse(code=404,message="Resource not found")
    })
    @PostMapping
    public ResponseEntity<Image> createImage(@RequestParam("file") MultipartFile imageFile, @RequestParam Long idPerson) throws IOException {
        return ResponseEntity.ok().body(imageRepository.createImage(imageFile,idPerson));
    }
}
