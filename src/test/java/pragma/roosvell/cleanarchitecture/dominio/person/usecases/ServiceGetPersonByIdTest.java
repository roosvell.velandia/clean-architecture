package pragma.roosvell.cleanarchitecture.dominio.person.usecases;

import org.junit.jupiter.api.Test;
import pragma.roosvell.cleanarchitecture.application.person.PersonRepository;
import pragma.roosvell.cleanarchitecture.data.DataPerson;
import pragma.roosvell.cleanarchitecture.dominio.person.model.Person;
import pragma.roosvell.cleanarchitecture.infrastructure.error.NotFoundExc;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ServiceGetPersonByIdTest {

    PersonRepository repository = mock(PersonRepository.class);
    ServiceGetPersonById service = new ServiceGetPersonById(repository);

    @Test
    void getPersonById() {
        when(repository.getPersonById(1L)).thenReturn(DataPerson.createPerson1().get());
        when(repository.getPersonById(2L)).thenReturn(DataPerson.createPerson2().get());

        Person person1 = service.getPersonById(1L);
        Person person2 = service.getPersonById(2L);
        //PersonasDTOResponse persona3 = servicio.getPersonaById(3L);


        assertSame(1L,person1.getId());
        assertSame(2L,person2.getId());

        verify(repository, times(2)).getPersonById(1L);
        verify(repository, times(2)).getPersonById(2L);
        assertThrows(NotFoundExc.class, ()->{
            service.getPersonById(3L);
        });
        verify(repository, times(5)).getPersonById(any(Long.class));
    }
}