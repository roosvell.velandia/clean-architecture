package pragma.roosvell.cleanarchitecture.infrastructure.image.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.multipart.MultipartFile;
import pragma.roosvell.cleanarchitecture.application.image.ImageRepository;
import pragma.roosvell.cleanarchitecture.data.DataImage;
import pragma.roosvell.cleanarchitecture.data.DataPerson;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ControllerCreateImage.class)
class ControllerCreateImageTest {

    @MockBean
    ImageRepository service;

    @Autowired
    private MockMvc mvc;


    @Test
    void createImage() throws Exception {
        when(service.createImage(any(),anyLong()))
                .thenReturn(DataImage.createImage1().get());
        mvc.perform(multipart("/api/image").file((MockMultipartFile) DataImage.createMultipartfile())
                        .contentType(MediaType.APPLICATION_JSON)
                        .queryParam("idPerson", String.valueOf(1L)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._id").value("id1"))
                .andDo(print());
    }
}