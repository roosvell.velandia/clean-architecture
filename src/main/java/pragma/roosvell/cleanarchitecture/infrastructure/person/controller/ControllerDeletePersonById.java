package pragma.roosvell.cleanarchitecture.infrastructure.person.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pragma.roosvell.cleanarchitecture.application.person.PersonRepository;
import pragma.roosvell.cleanarchitecture.infrastructure.error.NotFoundExc;

@RestController
@RequestMapping("/api/person")
@RequiredArgsConstructor
public class ControllerDeletePersonById {

    @Autowired
    PersonRepository personRepository;

    @ApiOperation(value="API to delete people in MySQL database",response= String.class)
    @ApiResponses(value={
            @ApiResponse(code=200,message="Person deleted correctly",response=String.class),
            @ApiResponse(code=204,message="Person deleted"),
            @ApiResponse(code=400,message="Person not found"),
            @ApiResponse(code=401,message="User unauthorized"),
            @ApiResponse(code=403,message="Operation forbidden"),
            @ApiResponse(code=404,message="Resource not found")
    })
    @DeleteMapping("{id}")
    public ResponseEntity<String> deletePersonById(Long id) throws NotFoundExc {
            return ResponseEntity.ok().body( personRepository.deletePerson(id));
    }
}
