package pragma.roosvell.cleanarchitecture.application.mapper;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import pragma.roosvell.cleanarchitecture.application.dto.PersonDTO;
import pragma.roosvell.cleanarchitecture.dominio.person.model.Person;
import pragma.roosvell.cleanarchitecture.infrastructure.person.entities.PersonEntity;
import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface PersonMapper {
    PersonEntity personToEntity (Person person);
    Person entityToPerson (PersonEntity personEntity);
    Person personDtoToPerson (PersonDTO personDTO);
    List<Person> listEntityToListPerson (List<PersonEntity> listEntity);
    List<PersonEntity> listPersonToListEntity (List<Person> listPerson);
}
