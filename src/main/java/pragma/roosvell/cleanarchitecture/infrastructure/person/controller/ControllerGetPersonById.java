package pragma.roosvell.cleanarchitecture.infrastructure.person.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pragma.roosvell.cleanarchitecture.application.person.PersonRepository;
import pragma.roosvell.cleanarchitecture.dominio.person.model.Person;
import pragma.roosvell.cleanarchitecture.infrastructure.error.NotFoundExc;

@RestController
@RequestMapping(value = "/api/person", produces = {MediaType.APPLICATION_JSON_VALUE})
@RequiredArgsConstructor
public class ControllerGetPersonById {

    @Autowired
    PersonRepository personRepository;

    @ApiOperation(value="API to Get people by id in MySQL database",response=Person.class)
    @ApiResponses(value={
            @ApiResponse(code=200,message="Person Details Retrieved",response=Person.class),
            @ApiResponse(code=201,message="Query well executed"),
            @ApiResponse(code=400,message="Person not found"),
            @ApiResponse(code=401,message="User unauthorized"),
            @ApiResponse(code=403,message="Operation forbidden"),
            @ApiResponse(code=404,message="Resource not found")
    })
    @GetMapping("{id}")
    public ResponseEntity<Person> getPersonById(Long id) throws NotFoundExc {
            return ResponseEntity.ok().body(personRepository.getPersonById(id));
    }
}