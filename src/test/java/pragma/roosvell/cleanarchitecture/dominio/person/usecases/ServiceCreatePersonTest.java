package pragma.roosvell.cleanarchitecture.dominio.person.usecases;

import org.junit.jupiter.api.Test;
import pragma.roosvell.cleanarchitecture.application.person.PersonRepository;
import pragma.roosvell.cleanarchitecture.data.DataPerson;
import pragma.roosvell.cleanarchitecture.dominio.person.model.Person;
import pragma.roosvell.cleanarchitecture.infrastructure.error.NotFoundExc;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;


class ServiceCreatePersonTest {

    PersonRepository repository = mock(PersonRepository.class);
    ServiceCreatePerson service = new ServiceCreatePerson(repository);

    @Test
    void createPerson() {
        repository.createPerson(DataPerson.createPerson1().get());
        assertDoesNotThrow( ()->{
            service.createPerson(DataPerson.createPerson1().get());
        });
    }
}