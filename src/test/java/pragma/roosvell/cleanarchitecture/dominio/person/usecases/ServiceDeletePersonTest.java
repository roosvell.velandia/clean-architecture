package pragma.roosvell.cleanarchitecture.dominio.person.usecases;

import javassist.NotFoundException;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import pragma.roosvell.cleanarchitecture.application.person.PersonRepository;
import pragma.roosvell.cleanarchitecture.data.DataPerson;
import pragma.roosvell.cleanarchitecture.infrastructure.error.NotFoundExc;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

@SpringBootTest
class ServiceDeletePersonTest {
    @MockBean
    PersonRepository repository;

    @Autowired
    ServiceDeletePerson service;
    @Test
    void deletePeople() throws Exception {
        when(repository.getPersonById(1L)).thenReturn(DataPerson.createPerson1().get());
        System.out.println(service.deletePerson(1L));
        assertThrows(NotFoundExc.class, ()->{
            System.out.println(service.deletePerson(3L));
        });
        verify(repository, times(1)).deletePerson(1L);
        verify(repository).deletePerson(any(Long.class));
    }

}