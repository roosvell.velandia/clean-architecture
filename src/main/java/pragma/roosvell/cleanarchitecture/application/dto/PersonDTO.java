package pragma.roosvell.cleanarchitecture.application.dto;

import lombok.*;

@Data
@Getter
@Setter
@RequiredArgsConstructor
public class PersonDTO {
    private Long dni;
    private String name;
    private String lastname;
}
