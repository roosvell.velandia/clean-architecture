package pragma.roosvell.cleanarchitecture.data;

import lombok.*;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;
import pragma.roosvell.cleanarchitecture.dominio.image.model.Image;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Data
@Getter
@Setter
public class DataImage {
    public static List<Image> createImages() {
        return Arrays.asList(new Image("id1","abcd",DataPerson.createPerson1().get()),
                new Image("id2","dcba",DataPerson.createPerson2().get()));
    }

    public static final Optional<Image> createImage1(){
        return Optional.of(new Image("id1","abcd",DataPerson.createPerson1().get()));
    }

    public static final Optional<Image> createImage2(){
        return Optional.of(new Image("id2","dcba",DataPerson.createPerson2().get()));
    }

    public static final Optional<Image> createImage3(){
        return Optional.of(new Image("id3","xyz",DataPerson.createPerson3().get()));
    }

    public static final MultipartFile createMultipartfile() throws IOException {
        FileInputStream path = new FileInputStream("C:\\Users\\roosvell.velandia\\IdeaProjects\\persona-imagen-V1\\src\\test\\java\\com\\backend\\monolito\\datos\\image.png");
        MockMultipartFile multipartFile = new MockMultipartFile("file", path);

        return multipartFile;
    }
}
