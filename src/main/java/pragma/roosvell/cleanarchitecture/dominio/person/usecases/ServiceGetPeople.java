package pragma.roosvell.cleanarchitecture.dominio.person.usecases;

import pragma.roosvell.cleanarchitecture.application.person.PersonRepository;
import pragma.roosvell.cleanarchitecture.dominio.person.model.Person;

import java.util.List;

public class ServiceGetPeople {
    private final PersonRepository personRepository;

    public ServiceGetPeople(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public List<Person> getPeople (){
        return this.personRepository.getPeople();
    }
}
