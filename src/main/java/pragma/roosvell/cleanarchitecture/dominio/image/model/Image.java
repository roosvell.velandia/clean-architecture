package pragma.roosvell.cleanarchitecture.dominio.image.model;

import pragma.roosvell.cleanarchitecture.dominio.person.model.Person;

public class Image {
    private String idImage;
    private String imageBase64;
    private Person person;

    public String get_id() {
        return idImage;
    }

    public void set_id(String idImage) {
        this.idImage = idImage;
    }

    public String getImageBase64() {
        return imageBase64;
    }

    public void setImageBase64(String imageBase64) {
        this.imageBase64 = imageBase64;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Image() {
    }

    public Image(String idImage, String imageBase64, Person person) {
        this.idImage = idImage;
        this.imageBase64 = imageBase64;
        this.person = person;
    }
}
