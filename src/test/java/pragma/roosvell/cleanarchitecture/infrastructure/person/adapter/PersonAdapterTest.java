package pragma.roosvell.cleanarchitecture.infrastructure.person.adapter;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import pragma.roosvell.cleanarchitecture.application.image.ImageRepository;
import pragma.roosvell.cleanarchitecture.application.mapper.PersonMapper;
import pragma.roosvell.cleanarchitecture.data.DataImage;
import pragma.roosvell.cleanarchitecture.data.DataPerson;
import pragma.roosvell.cleanarchitecture.dominio.image.model.Image;
import pragma.roosvell.cleanarchitecture.dominio.person.model.Person;
import pragma.roosvell.cleanarchitecture.infrastructure.error.NotFoundExc;
import pragma.roosvell.cleanarchitecture.infrastructure.person.entities.PersonEntity;
import pragma.roosvell.cleanarchitecture.infrastructure.person.persondao.PersonDao;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

@SpringBootTest
class PersonAdapterTest {

    @MockBean
    PersonDao repository;

    @MockBean
    ImageRepository imageRepository;

    @Autowired
    PersonMapper mapper;

    @Autowired
    PersonAdapter service;

    @Test
    void createPerson() {
        when(repository.save(mapper.personToEntity(DataPerson.createPerson1().get()))).thenReturn(mapper.personToEntity(DataPerson.createPerson1().get()));
        Person person1 = service.createPerson(DataPerson.createPerson1().get());
        assertEquals(person1.getDni(),DataPerson.createPerson1().get().getDni());
        assertEquals(person1.getName(),DataPerson.createPerson1().get().getName());
        assertEquals(person1.getLastname(),DataPerson.createPerson1().get().getLastname());
    }

    @Test
    void updatePerson() {
        when(repository.findById(1L))
                .thenReturn(Optional.ofNullable(mapper
                        .personToEntity(DataPerson.createPerson1().get())));
        when(repository.save(mapper.personToEntity(DataPerson.createPerson1().get())))
                .thenReturn(mapper.personToEntity(DataPerson.createPerson1().get()));
        Person person1 = service.updatePerson(DataPerson.createPerson1().get(),1L);
        assertEquals(person1.getName(),DataPerson.createPerson1().get().getName());
        assertEquals(person1.getLastname(),DataPerson.createPerson1().get().getLastname());
        assertEquals(person1.getDni(),DataPerson.createPerson1().get().getDni());
        assertThrows(RuntimeException.class, ()->{
            service.updatePerson(DataPerson.createPerson3().get(),3L);
        });
    }

    @Test
    void getPeople() {
        when(repository.findAll()).thenReturn(mapper.listPersonToListEntity(DataPerson.createPeople()));
        when(repository.findById(1L)).thenReturn(Optional.ofNullable(mapper.personToEntity(DataPerson.createPerson1().get())));
        when(repository.findById(2L)).thenReturn(Optional.ofNullable(mapper.personToEntity(DataPerson.createPerson2().get())));
        List<Person> people = service.getPeople();
        System.out.println(people);
        for (Person person:people){
            service.getPersonById(person.getId());
            assertNotNull(person);
        }
    }

    @Test
    void getPersonById() {
        when(repository.findById(1L)).thenReturn(Optional.ofNullable(mapper.personToEntity(DataPerson.createPerson1().get())));
        when(repository.findById(2L)).thenReturn(Optional.ofNullable(mapper.personToEntity(DataPerson.createPerson2().get())));

        Person persona1 = service.getPersonById(1L);
        Person persona2 = service.getPersonById(2L);
        //PersonasDTOResponse persona3 = servicio.getPersonaById(3L);


        assertSame(1L,persona1.getId());
        assertSame(2L,persona2.getId());

        verify(repository, times(1)).findById(1L);
        verify(repository, times(1)).findById(2L);
        assertThrows(NotFoundExc.class, ()->{
            service.getPersonById(3L);
        });
        verify(repository, times(3)).findById(any(Long.class));

    }

    @Test
    void deletePerson() {
        when(repository.findById(1L))
                .thenReturn(Optional.ofNullable(mapper.personToEntity(DataPerson.createPerson1().get())));
        repository.delete(mapper.personToEntity(DataPerson.createPerson1().get()));
        System.out.println(service.deletePerson(1L));
        assertThrows(NotFoundExc.class, ()->{
            service.deletePerson(3L);
        });
        verify(repository).delete(any(PersonEntity.class));

        when(imageRepository.getImagesByPersonId(1L))
                .thenReturn((DataImage.createImages()));
        when(imageRepository.deleteImage("id1")).thenReturn("image deleted");
        when(imageRepository.deleteImage("id2")).thenReturn("image deleted");
        List<Image> images = imageRepository.getImagesByPersonId(1L);
        assertNotNull(images);
        for(Image image:images){
                assertEquals("image deleted", imageRepository.deleteImage(image.get_id()));
        }
    }
}